﻿using System.Windows.Forms;
using Finder.Properties;

namespace Finder
{
    public static class Notifyer
    {
        public static void NotifyIfBadPath()
        {
             MessageBox.Show(Resources.Notifyer_NotifyIfBadPath_Print_Current_Path);
        }

        public static void NotifyIfSearchCancelled()
        {
            MessageBox.Show(Resources.Notifyer_NotifyIfSearchCancelled_Search_Cancelled);
        }

        public static void ShowMessageBox(string text)
        {
            MessageBox.Show(text);
        }
    }
}
