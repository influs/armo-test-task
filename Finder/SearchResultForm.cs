﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using Finder.Properties;

namespace Finder
{
    public partial class SearchResultForm : Form
    {
        private readonly ParametersForm parametersForm;
        private CancellationTokenSource cancellationTokenSource = 
            new CancellationTokenSource();
        private volatile Stopwatch timer = new Stopwatch();

        public SearchResultForm()
        {
            InitializeComponent();
        }

        public SearchResultForm(ParametersForm form)
        {          
            InitializeComponent();
            searchResultTreeView.PathSeparator = @"\\";
            parametersForm = form;
            var files = FileSystem.FilesToCheck(Settings.Default.directory,
                String.Concat(Settings.Default.filePattern, "*"));
            AnalyzeFileContent(files);
        }

        private static void PopulateTreeView(TreeView treeView, IEnumerable<string> paths, char pathSeparator)
        {
            TreeNode lastNode = null;
            string subPathAgg;
            foreach (string path in paths)
            {
                subPathAgg = string.Empty;
                foreach (string subPath in path.Split(pathSeparator))
                {
                    subPathAgg += subPath + pathSeparator;
                    TreeNode[] nodes = treeView.Nodes.Find(subPathAgg, true);
                    if (nodes.Length == 0)
                        if(lastNode == null)
                            lastNode = treeView.Nodes.Add(subPathAgg, subPath);
                        else
                            lastNode = lastNode.Nodes.Add(subPathAgg, subPath);
                    else
                        lastNode = nodes[0];
                }
            }
        }

        public async void AnalyzeFileContent(IEnumerable<string> files)
        {
            List<string> findfilesPath = new List<string>();
            timer.Start();
            int countCompletedFiles = 0;
            foreach (var file in files)
            {     
                currentFilePathTextBox.Text = file;
                try
                {
                    countCompletedFiles++;
                    var textInCurrentFile = await FileSystem.ReadTextAsync(file, cancellationTokenSource);
                    countFilesTextBox.Text = countCompletedFiles.ToString();
                    if (textInCurrentFile)
                        findfilesPath.Add(file);
                    PopulateTreeView(searchResultTreeView, findfilesPath, '\\');
                }
                catch (OperationCanceledException)
                {
                    Notifyer.NotifyIfSearchCancelled();
                    timer.Stop();
                    break;
                }
                catch (Exception ex)
                {
                    Notifyer.ShowMessageBox(ex.Message);
                }
                elapsedTimeTextBox.Text = timer.ElapsedMilliseconds.ToString();
            }      

            if (searchResultTreeView.Nodes.Count == 0)
            {
                searchResultTreeView.Nodes.Add("Файлы, соответствующие критериям поиска не найдены.");
            }

            timer.Stop();
            currentFilePathTextBox.Text = String.Empty;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            parametersForm.Close();
        }

        private void StopButtonClick(object sender, EventArgs e)
        {
            if(cancellationTokenSource != null)
            {
                cancellationTokenSource.Cancel();    
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
            parametersForm.Show();
        }
    }
}
