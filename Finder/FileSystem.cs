﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Finder.Properties;

namespace Finder
{
    class FileSystem
    {
        public static IEnumerable<string> FilesToCheck(string path, string pattern)
        {
            return Directory.GetFiles(path, pattern, SearchOption.AllDirectories);
        }

        public static bool IsPath(string path)
        {
            if( Directory.Exists(path) )
                return true;
            
            return false;
        }

        public static async Task<bool> ReadTextAsync(string filePath, CancellationTokenSource cts)
        {
            cts.Token.ThrowIfCancellationRequested();
            using (StreamReader sourceStream = new StreamReader(filePath, Encoding.Default))
            {
                var source = await sourceStream.ReadToEndAsync();
                StringBuilder sb = new StringBuilder();
                sb.Append(source);
                if (source.Contains(Settings.Default.textInFile))
                    return true;

                return false;
            }
        }
    }
}
