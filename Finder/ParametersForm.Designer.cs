﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Finder
{
    partial class ParametersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.initialFolderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.directoryPathTextBox = new System.Windows.Forms.TextBox();
            this.filePatternLabel = new System.Windows.Forms.Label();
            this.textInFileLabel = new System.Windows.Forms.Label();
            this.directoryLabel = new System.Windows.Forms.Label();
            this.textInFileTextbox = new System.Windows.Forms.TextBox();
            this.filePatternTextbox = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(556, 82);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(74, 21);
            this.button1.TabIndex = 0;
            this.button1.Text = "Обзор";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.BrowserDialogClick);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 22);
            this.label1.TabIndex = 6;
            // 
            // directoryPathTextBox
            // 
            this.directoryPathTextBox.Location = new System.Drawing.Point(170, 82);
            this.directoryPathTextBox.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.directoryPathTextBox.Name = "directoryPathTextBox";
            this.directoryPathTextBox.Size = new System.Drawing.Size(380, 20);
            this.directoryPathTextBox.TabIndex = 1;
            // 
            // filePatternLabel
            // 
            this.filePatternLabel.Location = new System.Drawing.Point(31, 167);
            this.filePatternLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.filePatternLabel.Name = "filePatternLabel";
            this.filePatternLabel.Size = new System.Drawing.Size(119, 21);
            this.filePatternLabel.TabIndex = 3;
            this.filePatternLabel.Text = "Шаблон имени файла";
            this.filePatternLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textInFileLabel
            // 
            this.textInFileLabel.Location = new System.Drawing.Point(15, 242);
            this.textInFileLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.textInFileLabel.Name = "textInFileLabel";
            this.textInFileLabel.Size = new System.Drawing.Size(118, 21);
            this.textInFileLabel.TabIndex = 5;
            this.textInFileLabel.Text = "Текст в файле";
            this.textInFileLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // directoryLabel
            // 
            this.directoryLabel.Location = new System.Drawing.Point(15, 80);
            this.directoryLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.directoryLabel.Name = "directoryLabel";
            this.directoryLabel.Size = new System.Drawing.Size(135, 22);
            this.directoryLabel.TabIndex = 2;
            this.directoryLabel.Text = "Директория поиска";
            this.directoryLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textInFileTextbox
            // 
            this.textInFileTextbox.Location = new System.Drawing.Point(170, 243);
            this.textInFileTextbox.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.textInFileTextbox.Name = "textInFileTextbox";
            this.textInFileTextbox.Size = new System.Drawing.Size(380, 20);
            this.textInFileTextbox.TabIndex = 1;
            // 
            // filePatternTextbox
            // 
            this.filePatternTextbox.Location = new System.Drawing.Point(170, 168);
            this.filePatternTextbox.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.filePatternTextbox.Name = "filePatternTextbox";
            this.filePatternTextbox.Size = new System.Drawing.Size(380, 20);
            this.filePatternTextbox.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(316, 329);
            this.button2.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(188, 57);
            this.button2.TabIndex = 7;
            this.button2.Text = "Поиск";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.SearchClick);
            // 
            // ParametersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 560);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.filePatternTextbox);
            this.Controls.Add(this.textInFileTextbox);
            this.Controls.Add(this.directoryLabel);
            this.Controls.Add(this.textInFileLabel);
            this.Controls.Add(this.filePatternLabel);
            this.Controls.Add(this.directoryPathTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.Name = "ParametersForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Поиск";
            this.Load += new System.EventHandler(this.ParametersForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button button1;
        private FolderBrowserDialog initialFolderBrowser;
        private Label label1;
        private TextBox directoryPathTextBox;
        private Label filePatternLabel;
        private Label textInFileLabel;
        private Label directoryLabel;
        private TextBox textInFileTextbox;
        private TextBox filePatternTextbox;
        private Button button2;

    }
}

