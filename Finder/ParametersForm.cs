﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Finder.Properties;

namespace Finder
{
    public partial class ParametersForm : Form
    {
        public ParametersForm()
        {
            InitializeComponent();
        }

        private void BrowserDialogClick(object sender, EventArgs e)
        {
            var initialDirectoryBrowser =
                new FolderBrowserDialog {ShowNewFolderButton = false};

            if (initialDirectoryBrowser.ShowDialog() == DialogResult.OK)
            {
                directoryPathTextBox.Text = initialDirectoryBrowser.SelectedPath;
            } 
        }

        private void SaveParameters()
        {
            Settings.Default.directory = directoryPathTextBox.Text;
            Settings.Default.filePattern = filePatternTextbox.Text;
            Settings.Default.textInFile = textInFileTextbox.Text;
            Settings.Default.Save();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
           SaveParameters();
        }

        private void ParametersForm_Load(object sender, EventArgs e)
        {
            directoryPathTextBox.Text = Settings.Default.directory;
            filePatternTextbox.Text = Settings.Default.filePattern;
            textInFileTextbox.Text = Settings.Default.textInFile;
        }

        private void SearchClick(object sender, EventArgs e)
        {
            SaveParameters();
            if ( !FileSystem.IsPath(directoryPathTextBox.Text) )
               Notifyer.NotifyIfBadPath();
            else
            {
                SearchResultForm searchForm = new SearchResultForm(this);
                Hide();
                searchForm.Show();
            }
        }
    }
}
